/*
 * Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

use ir::inst::*;
use ir::shader::*;

pub struct Builder {
    insts: Vec<Instruction>,

    simd_width: u8,
    simd_group: u8,
}

impl Builder {
    pub fn new() -> Builder {
        Builder {
            insts: Vec::new(),
            simd_width: 16,
            simd_group: 0,
        }
    }

    fn add_inst(&mut self, op: Opcode, dst: Dst, srcs: Vec<Src>) {
        self.insts.push(Instruction {
            op: op,
            simd_width: self.simd_width,
            simd_group: self.simd_group,
            dst: dst,
            srcs: srcs,
        });
    }

    pub fn alu1(&mut self, op: Opcode, dst: Dst, src0: Src) {
        self.add_inst(op, dst, vec![src0]);
    }

    pub fn alu2(&mut self, op: Opcode, dst: Dst, src0: Src, src1: Src) {
        self.add_inst(op, dst, vec![src0, src1]);
    }

    pub fn jump(&mut self, op: Opcode) {
        self.add_inst(op, Dst::null(Type::UW), Vec::new());
    }

    pub fn make_shader(&mut self) -> Shader {
        let mut s = Shader {
            blocks: Vec::new(),
        };
        let mut b = Block {
            instructions: Vec::new(),
        };

        for mut inst in self.insts.drain(..) {
            let is_cf = inst.is_control_flow();
            b.instructions.push(inst);
            if is_cf {
                s.blocks.push(b);
                b = Block {
                    instructions: Vec::new(),
                }
            }
        }
        if !b.instructions.is_empty() {
            s.blocks.push(b);
        }

        s
    }

    pub fn instructions(&self) -> &Vec<Instruction> {
        &self.insts
    }
}
