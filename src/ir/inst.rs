/*
 * Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

use std::fmt;

#[derive(Debug)]
pub enum Opcode {
    MOV =       1,
    SEL =       2,
    NOT =       4,
    AND =       5,
    OR =        6,
    XOR =       7,
    SHR =       8,
    SHL =       9,
    ASR =       12,
    CMP =       16,
    CMPN =      17,
    CSEL =      18,  /**< Gen8+ */
    F32TO16 =   19,  /**< Gen7 only */
    F16TO32 =   20,  /**< Gen7 only */
    BFREV =     23,  /**< Gen7+ */
    BFE =       24,  /**< Gen7+ */
    BFI1 =      25,  /**< Gen7+ */
    BFI2 =      26,  /**< Gen7+ */
    JMPI =      32,
    IF =        34,
    IFF =       35,  /**< Pre-Gen6    */ /* Reused */
    ELSE =      36,
    ENDIF =     37,
    DO =        38,  /**< Pre-Gen6    */ /* Reused */
    WHILE =     39,
    BREAK =     40,
    CONTINUE =  41,
    HALT =      42,
    WAIT =      48,
    SEND =      49,
    SENDC =     50,
    SENDS =     51,  /**< Gen9+ */
    SENDSC =    52,  /**< Gen9+ */
    MATH =      56,  /**< Gen6+ */
    ADD =       64,
    MUL =       65,
    AVG =       66,
    FRC =       67,
    RNDU =      68,
    RNDD =      69,
    RNDE =      70,
    RNDZ =      71,
    MAC =       72,
    MACH =      73,
    LZD =       74,
    FBH =       75,  /**< Gen7+ */
    FBL =       76,  /**< Gen7+ */
    CBIT =      77,  /**< Gen7+ */
    ADDC =      78,  /**< Gen7+ */
    SUBB =      79,  /**< Gen7+ */
    SAD2 =      80,
    SADA2 =     81,
    DP4 =       84,
    DPH =       85,
    DP3 =       86,
    DP2 =       87,
    LINE =      89,
    PLN =       90,  /**< G45+ */
    MAD =       91,  /**< Gen6+ */
    LRP =       92,  /**< Gen6+ */
    NOP =       126,
}

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Type {
    B,
    UB,
    W,
    UW,
    D,
    UD,
    Q,
    UQ,
    V,
    UV,

    VF,
    HF,
    F,
    DF,
    NF,
}

pub struct LogicalRegSrc {
    pub reg: u32,
    pub comp: u8,
}

enum SrcImp {
    Null,

    ImmediateW(i16),
    ImmediateUW(u16),
    ImmediateD(i32),
    ImmediateUD(u32),
    ImmediateQ(i64),
    ImmediateUQ(u64),
    ImmediateV(u32),
    ImmediateUV(u32),
    //ImmediateHF(u16),
    ImmediateF(f32),
    ImmediateDF(f64),
    //ImmediateVF(u32),

    LogicalReg(LogicalRegSrc),
}

pub struct Src {
    typ: Type,
    imp: SrcImp,
}

impl Src {
    pub fn imm_w(data: i16) -> Src {
        Src {
            typ: Type::W,
            imp: SrcImp::ImmediateW(data),
        }
    }

    pub fn imm_uw(data: u16) -> Src {
        Src {
            typ: Type::UW,
            imp: SrcImp::ImmediateUW(data),
        }
    }

    pub fn imm_d(data: i32) -> Src {
        Src {
            typ: Type::D,
            imp: SrcImp::ImmediateD(data),
        }
    }

    pub fn imm_ud(data: u32) -> Src {
        Src {
            typ: Type::UD,
            imp: SrcImp::ImmediateUD(data),
        }
    }

    pub fn imm_q(data: i64) -> Src {
        Src {
            typ: Type::Q,
            imp: SrcImp::ImmediateQ(data),
        }
    }

    pub fn imm_uq(data: u64) -> Src {
        Src {
            typ: Type::UQ,
            imp: SrcImp::ImmediateUQ(data),
        }
    }

    pub fn imm_f(data: f32) -> Src {
        Src {
            typ: Type::F,
            imp: SrcImp::ImmediateF(data),
        }
    }

    pub fn imm_df(data: f64) -> Src {
        Src {
            typ: Type::DF,
            imp: SrcImp::ImmediateDF(data),
        }
    }

    pub fn lreg(typ: Type, reg: u32, comp: u8) -> Src {
        Src {
            typ: typ,
            imp: SrcImp::LogicalReg(LogicalRegSrc {
                reg: reg,
                comp: comp,
            }),
        }
    }

    pub fn null(typ: Type) -> Src {
        Src {
            typ: typ,
            imp: SrcImp::Null,
        }
    }

    pub fn validate(&self) {
        match self.imp {
            SrcImp::Null => (),
            SrcImp::ImmediateW(_imm) => assert!(self.typ == Type::W),
            SrcImp::ImmediateUW(_imm) => assert!(self.typ == Type::UW),
            SrcImp::ImmediateD(_imm) => assert!(self.typ == Type::D),
            SrcImp::ImmediateUD(_imm) => assert!(self.typ == Type::UD),
            SrcImp::ImmediateQ(_imm) => assert!(self.typ == Type::Q),
            SrcImp::ImmediateUQ(_imm) => assert!(self.typ == Type::UQ),
            SrcImp::ImmediateV(_imm) => assert!(self.typ == Type::V),
            SrcImp::ImmediateUV(_imm) => assert!(self.typ == Type::UV),
            //SrcImp::ImmediateHF(_imm) => assert!(self.typ == Type::HF),
            SrcImp::ImmediateF(_imm) => assert!(self.typ == Type::F),
            SrcImp::ImmediateDF(_imm) => assert!(self.typ == Type::DF),
            //SrcImp::ImmediateVF(_imm) => assert!(self.typ == Type::VF),

            SrcImp::LogicalReg(ref _reg) => {
            },
        }
    }
}

impl fmt::Display for Src {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.imp {
            SrcImp::Null => write!(f, "<null>{:?}", self.typ),
            SrcImp::ImmediateW(imm) => write!(f, "{}w", imm),
            SrcImp::ImmediateUW(imm) => write!(f, "0x{:04x}uw", imm),
            SrcImp::ImmediateD(imm) => write!(f, "{}d", imm),
            SrcImp::ImmediateUD(imm) => write!(f, "0x{:08x}ud", imm),
            SrcImp::ImmediateQ(imm) => write!(f, "{}q", imm),
            SrcImp::ImmediateUQ(imm) => write!(f, "0x{:016x}uq", imm),
            SrcImp::ImmediateV(imm) => write!(f, "0x{:08x}v", imm),
            SrcImp::ImmediateUV(imm) => write!(f, "0x{:08x}uv", imm),
            // SrcImp::ImmediateHF(imm)
            SrcImp::ImmediateF(imm) => write!(f, "{}f", imm),
            SrcImp::ImmediateDF(imm) => write!(f, "{}df", imm),
            // SrcImp::ImmediateVF(imm)

            SrcImp::LogicalReg(ref reg) => {
                write!(f, "lreg<{}.{}>{:?}", reg.reg, reg.comp, self.typ)
            },
        }
    }
}

pub struct LogicalRegDst {
    pub reg: u32,
    pub comp: u8,
    pub num_comps: u8,
}

pub enum DstImp {
    Null,
    LogicalReg(LogicalRegDst),
}

pub struct Dst {
    typ: Type,
    imp: DstImp,
}

impl Dst {
    pub fn lreg(typ: Type, reg: u32, comp: u8, num_comps: u8) -> Dst {
        Dst {
            typ: typ,
            imp: DstImp::LogicalReg(LogicalRegDst {
                reg: reg,
                comp: comp,
                num_comps: num_comps,
            }),
        }
    }

    pub fn null(typ: Type) -> Dst {
        Dst {
            typ: typ,
            imp: DstImp::Null,
        }
    }

    pub fn validate(&self) {
    }
}

impl fmt::Display for Dst {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.imp {
            DstImp::Null => write!(f, "<null>{:?}", self.typ),
            DstImp::LogicalReg(ref reg) => {
                write!(f, "lreg<{}.{}>{:?}", reg.reg, reg.comp, self.typ)
            },
        }
    }
}

pub struct Instruction {
    pub op: Opcode,

    pub simd_width: u8,
    pub simd_group: u8,

    pub srcs: Vec<Src>,
    pub dst: Dst,
}

impl Instruction {
    pub fn is_control_flow(&self) -> bool {
        match self.op {
            Opcode::JMPI        => true,
            Opcode::IF          => true,
            Opcode::IFF         => true,
            Opcode::ELSE        => true,
            Opcode::ENDIF       => true,
            Opcode::DO          => true,
            Opcode::WHILE       => true,
            Opcode::BREAK       => true,
            Opcode::CONTINUE    => true,
            Opcode::HALT        => true,
            _                   => false,
        }
    }

    pub fn validate(&self) {
        self.dst.validate();
        for ref src in &self.srcs {
            src.validate();
        }
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}({})", self.op, self.simd_width)?;
        write!(f, " {}", self.dst)?;
        for ref src in &self.srcs {
            write!(f, " {}", src)?;
        }
        Ok(())
    }
}
